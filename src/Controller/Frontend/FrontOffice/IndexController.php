<?php

namespace App\Controller\Frontend\FrontOffice;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

 /**
 * Class IndexController
 * @package App\Controller\Frontend\FrontOffice
 */

class IndexController extends AbstractController
{
    /**
    * @Route("/", name="frontend_frontoffice_index")
    */

    public function indexAction(): Response
    {
             
        return $this->render('frontend/frontoffice/index.html.twig', [
            'controller_name' => 'Frontoffice'
        ]);
    }
}
