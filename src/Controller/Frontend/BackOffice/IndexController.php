<?php

namespace App\Controller\Frontend\BackOffice;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

 /**
 * Class IndexController
 * @package App\Controller\Frontend\BackOffice
 */

class IndexController extends AbstractController
{
    /**
    * @Route("/backoffice", name="app_admin_index")
    */

    public function indexAction(): Response
    {
          return $this->render('frontend/backoffice/index.html.twig', [
            'controller_name' => 'Backoffice',
        ]);
    }
}
