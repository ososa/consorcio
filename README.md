## Instalación Bootstrap en Symfony 5 con webpack encore

> Fuente de informacion [Symfony 5 La Vía Rápida | Paso 22 - Dando estilos a la interfaz con Webpack](https://www.youtube.com/watch?v=dhvsVB0I6p0)

1. Instalar `npm`

1. `symfony composer require encore`

Crea la carpeta `assets` en symfony (se necesita configurar los archivos SASS)

1. `npm add bootstrap jquery @popperjs/core bs-custom-file-input --dev`

Crear la carpeta `node_modules`

1. Configurar `webpack.config.js` y `assets/styles/app.scss`

1. Crear el webpack encore con `npm run build`

Este crear el js. .css en la carpeta `public/build`

1. Luego agregar a tu `template/base.html.twig`

 ```
 .....(css al inicio)

{% block stylesheets %}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,500,500i,600,600i&display=swap" rel="stylesheet" />
    {{ encore_entry_link_tags('app') }}
{% endblock %} 

.....(javascript al final) 

{% block javascripts %}
    {{ encore_entry_script_tags('app') }}
{% endblock %}

 ```
## Instalación tabla usuarios backoffice

1. sudo composer require symfonycasts/reset-password-bundle

1. composer require gedmo/doctrine-extension

1. php bin/console make:migration

1. php bin/console doctrine:migration:migrate

## Uso de include y extends en twig

1. [Crear template con teig](https://www.youtube.com/watch?v=vndJLEb-jSA)



